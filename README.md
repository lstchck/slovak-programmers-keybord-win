# Slovak Programmers Keybord Win

Klávesnica inšpirovaná layoutom Czech Programmers, ktorý je priamo integrovaný vo WIN. Slovenske znaky sa píšu cez pravý Alt (Shift + Alt pre kapitálky)

Edit bol robený cez [Keyboard layout creator](https://www.microsoft.com/en-us/download/details.aspx?id=102134)

Source file nájdeš v [repe](./Slovak%20Programmers.klc) - uvítam všetky vylepšenia :)

## AltGr

![altgr](assets/altgr.png)

## Shift + AltGr

![shiftaltgr](assets/shift-altgr.png)

## Ak by bol problém s odinštalovaním

[https://social.msdn.microsoft.com/Forums/ie/en-US/6e143a03-3fda-43fd-831b-2c3056d732b1/how-do-i-remove-a-keyboard-layout?forum=windowsuidevelopment](https://social.msdn.microsoft.com/Forums/ie/en-US/6e143a03-3fda-43fd-831b-2c3056d732b1/how-do-i-remove-a-keyboard-layout?forum=windowsuidevelopment)
